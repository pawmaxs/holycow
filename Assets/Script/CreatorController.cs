using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatorController : MonoBehaviour
{
    public float speed = 30.0f;
    public GameObject HolyCowPrefabs;
    public float forwardInput;

    void Update()
    {
        // On spacebar press, send HolyCow
        if (Input .GetKeyDown(KeyCode.Space))
        {
            Instantiate(HolyCowPrefabs, transform.position, transform.rotation);
        }
    }
}
