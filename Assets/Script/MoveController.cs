using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    private float speed = 5;
    private Vector3 _direction = Vector3.right;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Wall")
        {
            _direction = Vector3.back;
        }
    }
    // Update is called once per frame
    void Update()
    {
        transform.Translate(_direction * Time.deltaTime * speed);
    }
}
